import inquirer from "inquirer";

export default async (choices = []) => inquirer.prompt([
  {
    type: 'list',
    name: 'tplModule',
    message: '请选择模板',
    choices
  }
])
